extends "res://engine/entity.gd"

var state = "default"
var keys = 0

func _ready():
	set_collision_mask_bit(1, 0)
	TYPE = "PLAYER"
	SPEED = 70
	MAXHEALTH = 16
	health = MAXHEALTH
	set_physics_process(true)


func _physics_process(_delta):
	match state:
		"default":
			state_default()
		"swing":
			state_swing()
	
	keys = min(keys, 9)

func state_default():
	controls_loop()
	movement_loop()
	spritedir_loop()
	damage_loop()
	
	if movedir == Vector2.ZERO:
		anim_switch("idle")
	elif is_on_wall():
		if spritedir == "left" and test_move(transform, Vector2(-1, 0)):
			anim_switch("push")
		if spritedir == "right" and test_move(transform, Vector2(1, 0)):
			anim_switch("push")
		if spritedir == "up" and test_move(transform, Vector2(0, -1)):
			anim_switch("push")
		if spritedir == "down" and test_move(transform, Vector2(0, 1)):
			anim_switch("push")		
	else:
		anim_switch("walk")
		
	if Input.is_action_just_pressed("a"):
		use_item(preload("res://items/sword.tscn"))
		
	
func state_swing():
	anim_switch("idle")
	movement_loop()
	damage_loop()
	movedir = dir.center
	
	
func controls_loop():
	var LEFT = Input.is_action_pressed("ui_left")
	var RIGHT = Input.is_action_pressed("ui_right")
	var UP = Input.is_action_pressed("ui_up")
	var DOWN = Input.is_action_pressed("ui_down")
	
	movedir.x = -int(LEFT) + int(RIGHT)
	movedir.y = -int(UP) + int(DOWN)
